set nu
set relativenumber
set tabstop=4 softtabstop=4
set incsearch
set nohlsearch
set scrolloff=8
set signcolumn=yes
set colorcolumn=80


call plug#begin('~/.vim/plugged')
Plug 'ghifarit53/tokyonight-vim'
call plug#end()


let g:tokyonight_transparent_background=1
colorscheme tokyonight

